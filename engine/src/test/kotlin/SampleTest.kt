package com.qiubix.draft

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SampleTest {

  @Test
  fun `sample test case`() {
    assertThat(1 + 2).isEqualTo(3)
  }
}
