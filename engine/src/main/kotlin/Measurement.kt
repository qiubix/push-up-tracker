package com.syncron.supplychain.pushuptracker

data class Measurement (
        val userName: String,
        val pushupsCount: Int
)
