package com.syncron.supplychain.pushuptracker

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class PushupController {

    val userM: UserData = UserData("Milosz", 30000, 30000, 30000.0)
    val userA: UserData = UserData(userName = "Agata", dailySum = 200, totalSum = 2000, dailyAverage = 200.0)
    val userP: UserData = UserData(userName = "Paweł", dailySum = 100, totalSum = 1000, dailyAverage = 100.0)
    val users: HashMap<String, UserData> = mapOf(Pair("Agata", userA), Pair("Paweł", userP), Pair("Milosz", userM)) as HashMap<String, UserData>

    @GetMapping("/getAll")
    fun getAll() = users.values.toList()

    @PostMapping("/addNew")
    fun updateUserData(@RequestBody measurement: Measurement) : UserData? {
        val userName = measurement.userName
        if (!users.containsKey(userName)) {
            val updatedUser = UserData(userName, measurement.pushupsCount, measurement.pushupsCount, measurement.pushupsCount.toDouble())
            users.put(userName, updatedUser)
            return updatedUser
        } else {
            val user = users.get(userName) ?: throw IllegalStateException()
            val updatedDailySum = user.dailySum + measurement.pushupsCount
            val updatedTotalSum = user.totalSum + measurement.pushupsCount
            val updatedDailyAverage = 0.0
            val updatedUser = UserData(userName, updatedDailySum, updatedTotalSum, updatedDailyAverage)
            users.put(userName, updatedUser)
            return updatedUser
        }
    }
}
