package com.syncron.supplychain.pushuptracker

data class UserData(
    val userName: String,
    val dailySum: Int,
    val totalSum: Int,
    val dailyAverage: Double
)
