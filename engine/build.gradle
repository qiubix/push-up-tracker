buildscript {
  ext {
    kotlinVersion = '1.3.11'
    springBootVersion = '2.0.4.RELEASE'
    swaggerVersion = "2.9.2"
  }
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
    classpath 'org.liquibase:liquibase-core:3.5.3'
    classpath 'org.postgresql:postgresql:42.1.4'
    classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${kotlinVersion}")
    classpath("org.jetbrains.kotlin:kotlin-allopen:${kotlinVersion}")
    classpath "org.jetbrains.kotlin:kotlin-noarg:${kotlinVersion}"
  }
}

plugins {
  id 'org.liquibase.gradle' version '1.2.4'
  id 'org.jetbrains.kotlin.jvm' version '1.3.11'
}

repositories {
  mavenCentral()
}

apply plugin: 'kotlin-spring'
apply plugin: 'kotlin-jpa'
apply plugin: 'org.springframework.boot'
apply plugin: 'io.spring.dependency-management'
apply plugin: 'idea'
apply plugin: 'jacoco'

version = '1.0.0-SNAPSHOT'
sourceCompatibility = 1.8
targetCompatibility = 1.8

sourceSets {
  verify {
    kotlin {
      compileClasspath += main.output + main.compileClasspath
      compileClasspath += test.output + test.compileClasspath
      runtimeClasspath += test.runtimeClasspath
    }
    resources.srcDir file('src/verify/resources')
  }
}

tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile).all {
  kotlinOptions {
    jvmTarget = "1.8"
    freeCompilerArgs = ["-Xjsr305=strict"]
  }
}

dependencies {
  implementation 'org.jetbrains.kotlin:kotlin-stdlib'
  implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
  implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
  implementation 'org.springframework.boot:spring-boot-starter-data-rest'
  implementation 'org.springframework.boot:spring-boot-starter-jdbc'
  implementation 'org.springframework.boot:spring-boot-starter-web'
  implementation 'org.springframework.boot:spring-boot-starter-actuator'
  //implementation 'org.springframework.boot:spring-boot-starter-security'
  implementation 'com.google.gag:gag:1.0.1'
//  implementation 'org.liquibase:liquibase-core:3.5.3'
//  implementation 'org.hibernate:hibernate-java8:5.1.0.Final'
  implementation 'com.fasterxml.jackson.datatype:jackson-datatype-jsr310'
  implementation 'com.fasterxml.jackson.module:jackson-module-kotlin'
//  implementation 'ru.yandex.qatools.embed:postgresql-embedded:2.10'
  implementation "io.springfox:springfox-swagger2:${swaggerVersion}"
  implementation "io.springfox:springfox-swagger-ui:${swaggerVersion}"

  runtime 'com.h2database:h2'
//  runtime 'org.postgresql:postgresql'

  runtime('org.springframework.boot:spring-boot-devtools')

  testImplementation('org.springframework.boot:spring-boot-starter-test') {
    exclude group:'com.vaadin.external.google', module: 'android-json'
    exclude group: 'junit', module: 'junit'
  }

  testImplementation 'org.junit.platform:junit-platform-launcher:1.3.2'
  testImplementation 'org.junit.jupiter:junit-jupiter-api:5.2.0'
  testImplementation 'org.junit.jupiter:junit-jupiter-params:5.2.0'
//  testImplementation ('org.springframework.security:spring-security-test:5.0.0.RELEASE') {
//    exclude group: 'junit', module: 'junit'
//  }
  testImplementation 'org.assertj:assertj-core:3.9.1'
//  testImplementation 'org.testcontainers:postgresql:1.10.2'
//  testImplementation 'org.testcontainers:junit-jupiter:1.10.2'
  testRuntime 'org.junit.jupiter:junit-jupiter-engine:5.2.0'
}

bootJar {
  dependsOn ':webapp:buildClient'

  into('BOOT-INF/classes/static') {
    from "${project(':webapp').projectDir}/dist"
  }
}

test {
  useJUnitPlatform()
  testLogging {
    events "passed", "skipped", "failed"
    exceptionFormat = 'full'
    afterSuite {
      desc, result -> reportTestResults(desc, result)
    }
  }
  outputs.upToDateWhen { false }
  reports {
    html.enabled = true
  }
}

task verify(type: Test) {
  useJUnitPlatform()
  testClassesDirs = sourceSets.verify.output.classesDirs
  classpath = sourceSets.verify.runtimeClasspath
  outputs.upToDateWhen { false }
  testLogging {
    events "passed", "skipped", "failed"
    exceptionFormat = 'full'
    afterSuite {
      desc, result -> reportTestResults(desc, result)
    }
  }
  reports {
    html.enabled = true
  }
}

check.dependsOn verify

private void reportTestResults(desc, result) {
  if (!desc.parent) { // will match the outermost suite
    println "Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} successes, ${result.failedTestCount} failures, ${result.skippedTestCount} skipped)"
  }
}

jacocoTestReport {
  reports {
    xml.enabled true
  }
  executionData test,verify
}

tasks.withType(Test) {
  jacoco {
    destinationFile = file("${buildDir}/jacoco/test.exec")
  }
}

task cleanJacoco(dependsOn: 'clean') {
  delete "${buildDir}/jacoco"
}

check.dependsOn jacocoTestReport

bootRun {
  systemProperties System.properties
}

task stage {
  dependsOn build
}

gradle.taskGraph.whenReady { graph ->
  if (graph.hasTask(stage)) {
    test.enabled = false
    verify.enabled = false
  }
}

idea {
  module {
    inheritOutputDirs = false
    outputDir = file(project.buildDir.toString() + '/classes/main/')
    testSourceDirs += file('verify')
    testSourceDirs += project.sourceSets.verify.kotlin.srcDirs
    testSourceDirs += project.sourceSets.verify.resources.srcDirs
  }
}
