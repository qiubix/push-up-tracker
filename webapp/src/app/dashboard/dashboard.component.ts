import { Component, OnInit } from '@angular/core';
import { AddPushupsDialogComponent } from '../add-pushups-dialog/add-pushups-dialog.component';
import { MatDialog } from '@angular/material';
import { DashboardEntry } from '../model/dashboard-entry';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { NewEntry } from '../model/new-entry.model';

// const ELEMENT_DATA: DashboardEntry[] = [
//   { userName: 'Paweł', dailySum: 100, totalSum: 10, dailyAverage: 10 },
//   { userName: 'Agata', dailySum: 90, totalSum: 100, dailyAverage: 24 }
// ];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['user', 'dailySum', 'totalSum', 'dailyAverage'];
  dataSource: DashboardEntry[] = [];

  getAllEndpoint = environment.apiUrl + '/getAll';
  updateEndpoint = environment.apiUrl + '/addNew';

  constructor(public dialog: MatDialog, private http: HttpClient) {}

  ngOnInit() {
    this.getAllPushups().subscribe(data => (this.dataSource = data));
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddPushupsDialogComponent, {
      data: { userName: '', pushupsCount: '' }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log('Result: ', result);
      this.updatePushups(result).subscribe(updatedEntry => {
        this.dataSource = this.dataSource.map(oldEntry =>
          oldEntry.userName === updatedEntry.userName ? updatedEntry : oldEntry
        );
        if (!this.dataSource.includes(updatedEntry)) {
          this.dataSource.push(updatedEntry);
        }
      });
    });
  }

  getAllPushups(): Observable<DashboardEntry[]> {
    return this.http.get<DashboardEntry[]>(this.getAllEndpoint);
  }

  updatePushups(data: NewEntry) {
    console.log('Data: ', data);
    return this.http.post<DashboardEntry>(this.updateEndpoint, data);
  }
}
