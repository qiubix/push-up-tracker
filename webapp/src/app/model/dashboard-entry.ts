export interface DashboardEntry {
  userName: string;
  dailySum: number;
  totalSum: number;
  dailyAverage: number;
}
