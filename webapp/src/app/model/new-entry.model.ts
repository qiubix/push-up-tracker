import { FormGroup } from '@angular/forms';

export class NewEntry {
  userName: string;
  pushupsCount: number;

  public static mapFromForm(form: FormGroup) {
    let entry = new NewEntry();
    entry.userName = form.get('userName').value;
    entry.pushupsCount = form.get('pushupsCount').value;
    return entry;
  }
}
