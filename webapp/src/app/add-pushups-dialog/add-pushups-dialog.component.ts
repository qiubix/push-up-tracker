import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NewEntry } from '../model/new-entry.model';

@Component({
  selector: 'app-add-pushups-dialog',
  templateUrl: './add-pushups-dialog.component.html',
  styleUrls: ['./add-pushups-dialog.component.css']
})
export class AddPushupsDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddPushupsDialogComponent>,
    fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: NewEntry
  ) {
    this.form = fb.group({
      userName: data.userName,
      pushupsCount: data.pushupsCount
    });
  }

  ngOnInit() {}

  submitPushups() {
    console.log('Pushups was submitted');
    this.dialogRef.close(NewEntry.mapFromForm(this.form));
  }
}
