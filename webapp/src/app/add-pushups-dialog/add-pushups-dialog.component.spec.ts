import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPushupsDialogComponent } from './add-pushups-dialog.component';

describe('AddPushupsDialogComponent', () => {
  let component: AddPushupsDialogComponent;
  let fixture: ComponentFixture<AddPushupsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddPushupsDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPushupsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
